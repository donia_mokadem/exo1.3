
SET JAVA_PATH="C:\Program Files\Java\jdk-13.0.1\bin\java"
SET PROJECT_PATH=C:\Users\tvonstebut\IdeaProjects\NFA035

cd %PROJECT_PATH%

%JAVA_PATH% -ea --class-path "bin;libs\commons-codec-1.14.jar" -Duser.dir=%PROJECT_PATH% fr.cnam.foad.nfa035.fileutils.streaming.test.StreamingTest

