
#/bin/sh

JAVA_PATH="/c/Program Files/Java/jdk-13.0.1/bin/java"
PROJECT_PATH=/c/Users/tvonstebut/IdeaProjects/NFA035
cd $PROJECT_PATH # nécessaire pour corriger le souci décrit ici: https://bugs.java.con/bugdatabase/view_bug.do;:YfiG?bug_id=4483097


"$JAVA_PATH" -ea --class-path "bin;libs/commons-codec-1.15.jar" fr.cnam.foad.nfa035.fileutils.simpleaccess.test.StreamingTest
